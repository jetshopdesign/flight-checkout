var app = {
    name: "Flight Checkout",
    version: "0.1.0",
    required: {
        "J": "1.4.6"
    },
    config: {
    },
    init: function () {
        // Add custom functions to each page type,
        // Please: Do not remove the J.component.* functions

        J.pages.addToQueue("all-pages", function () {
            J.components.jetshopLogo();
        });

        J.pages.addToQueue("orderconfirmation-page", function () {
            J.components.orderConfirm();
        });

        J.pages.addToQueue("checkout-page", function () {

        });

    },
};



J.pages.addToQueue("all-pages", app.init);