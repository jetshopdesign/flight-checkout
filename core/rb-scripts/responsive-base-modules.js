// Inserted by build-process, getSassBreakpoints()
J.config.breakpoints = {"small":640,"medium":1008,"large":1360,"xlarge":1600};

// 
//    Module: checkout-validation
//
var checkoutValidation = {
    config: {
        errorDivOffset: 2
    },
    init: function () {
        // Only if responsive checkout
        if (typeof Checkout != "undefined") {
            if (typeof (Page_ClientValidate) != "undefined") {
                ValidatorUpdateDisplay = checkoutValidation.validate;
            }
        }

        // // TEST
        // var view = J.views['checkout-validation/checkout-validation'];
        // $("body").append(view);
    },
    validate: function (val) {
        var that = $(val);
        if (val.isvalid) {
            that.hide();
        } else {
            that.fadeIn('fast');
            var labelHeight = that.outerHeight();
            var inputHeight = that.parent().find(".form-text").outerHeight();
            var setHeight = labelHeight + inputHeight + checkoutValidation.config.errorDivOffset;
            that.css("margin-top", -setHeight);
            that.parent().find(".form-text").addClass("active-error").focus(function () {
                checkoutValidation.clearError($(this));
            }).on("input", function () {
                checkoutValidation.clearError($(this));
            });
        }
    },
    clearError: function (that) {
        that.removeClass("active-error");
        that.parent().find(".customer-info-input-error").hide();
    }
};

J.pages.addToQueue("checkout-page", checkoutValidation.init);


// 
//    Module: checkout-back-button
//
var checkoutBackButton = {
    init: function () {

        checkoutBackButton.addTranslations();

        if($(".empty-cart-wrapper").length) {
            $("html").addClass("empty-cart-checkout")
        }

        // BIND DOM CHANGES IN CART TO RUN REWRITECHECKOUT
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            checkoutBackButton.rewriteResponsiveCheckout();
        });
        checkoutBackButton.rewriteResponsiveCheckout();


    },

    rewriteResponsiveCheckout: function(){
        var checkoutHeaderContainer = $("#ctl00_main_responsivecheckout_UpCheckoutHeader");
        var checkoutHeader = checkoutHeaderContainer.find("h1");
        if(!checkoutHeaderContainer.find(".checkout-top-links").length) {
            var topLinkContainer = $("<div class='checkout-top-links'></div>");
            checkoutHeader.find("a").appendTo(topLinkContainer);
            topLinkContainer.prepend("<a class='button checkout-back-button' href='" + JetshopData.Urls.CountryRootUrl + "'>"
                + J.translate("checkoutBackButtonText") + "</a>");
            checkoutHeaderContainer.append(topLinkContainer);
        }
    },
    addTranslations: function () {
        J.translations.push({
            checkoutBackButtonText: {
                sv: "Fortsätt handla",
                nb: "Fortsette å handle",
                da: "Fortsætte med at handle",
                fi: "Jatka ostoksia",
                de: "Mit dem Einkaufen fortfahren",
                en: "Continue shopping"
            }
        });
    }


};

J.pages.addToQueue("checkout-page", checkoutBackButton.init);
