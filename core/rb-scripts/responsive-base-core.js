/* jshint -W057 */
/* jshint -W058 */

//
//  "J"
//

var J = new function () {
    "use strict";

    this.name = "Jetshop Responsive Base Framework";
    this.version = "1.4.6";
    this.responsive = true; // use bool J.responsive to determine if framework is present
    this.cart = false;
    this.data = {
        currency: false,
        culture: false,
        priceList: false,
        priceListId: false,
        categoryId: false,
        productId: false,
        language: false,
        manufacturerList: false,
        jetshopData: false
    };
    this.config = {
        cssFallbackUrl: "/M1/Production/css/responsive-base.css", // Used for injection in pages lacking script management, not a part of JetshopData.Urls
        breakpoints: [], // Will be added from build-process
        apiCachePrefix: "cache-",
        foundationConfig: {
            equalizer: {
                equalize_on_stack: true
            }
        },
        sameHeightConfig: {
            byRow: true,
            property: 'height',
            target: null,
            remove: false
        },
        urls: {
            "CountryRootUrl": "/",
            "CheckoutUrl": "/checkout",
            "StageCheckoutUrl": "/stage/checkout",
            "ServicesUrl": "/Services",
            "MyPagesUrl": "/my-pages",
            "StageMyPagesUrl": "/stage/my-pages",
            "LoginUrl": "/login",
            "StageLoginUrl": "/stage/login",
            "SignupUrl": "/signup",
            "SignoutUrl": "/signout",
            "MyPagesOrdersUrl": "/my-pages/orders",
            "MyPagesOrderdetailsUrl": "/my-pages/orderdetails",
            "MyPagesSettingsUrl": "/my-pages/settings",
            "TermsAndConditionsUrl": "kopvillkor",
            "CartServiceUrl": "/Services/CartInfoService.asmx/LoadCartInfo"
        }
    };
    this.checker = {
        isStage: false,
        isLoggedIn: false,
        isTouch: false,
        isStartPage: false,
        isProductPage: false,
        isCategoryPage: false,
        isCategoryAdvancedPage: false,
        isNewsPage: false,
        isSearchResultPage: false,
        isCheckoutPage: false,
        isStandardPage: false,
        isOrderConfirmationPage: false,
        isManufacturerPage: false,
        isManufacturerAdvancedPage: false,
        isSiteMapPage: false,
        isMyPage: false,
        isVatIncluded: false,
        isCheckoutHttps: false
    };
    this.translations = [
        {
            cart: {
                sv: "Kundvagn",
                en: "Cart",
                da: "Indkøbskurv",
                nb: "Handlevogn",
                fi: "Ostoskärry"
            }
        }
    ];
    this.translate = function (translationObject) {
        for (var key in J.translations) {
            if (translationObject in J.translations[key]) {
                if (typeof J.translations[key][translationObject][J.data.language] !== "undefined") {
                    return J.translations[key][translationObject][J.data.language];
                }
                else if (typeof J.translations[key][translationObject]["en"] !== "undefined") {
                    console.log("No translation found for '" + translationObject + "' in language " + J.data.language + ". Returning English.");
                    return J.translations[key][translationObject]["en"];
                }
                else if (typeof J.translations[key][translationObject]["sv"] !== "undefined") {
                    console.log("No translation found for '" + translationObject + "' in language " + J.data.language + ". Returning Swedish.");
                    return J.translations[key][translationObject]["sv"];
                } else {
                    console.log("No translation found for '" + translationObject + "' in language " + J.data.language);
                }
            }
        }
        console.log("No translation found for " + translationObject + "'");
        return false;
    };
    this.setPageType = function () {
        if (typeof JetshopData !== "undefined") {
            if ($("html.page-responsive-checkout").length || $("html.page-responsive-mobile-checkout").length) {
                this.checker.isCheckoutPage = true;
                return "checkout-page";
            } else if ($("html.page-responsive-orderconfirmed").length || $("html.page-responsive-mobile-orderconfirmed").length) {
                this.checker.isOrderConfirmationPage = true;
                return "orderconfirmation-page";
            } else {
                // Default pageType
                this.checker.isStandardPage = true;
                return "standard-page";
            }
        } else {
            return false;
        }
    };
    this.switch = {
        width: "",
        queues: {
            small: [],
            mediumUp: [],
            medium: [],
            largeUp: []
        },
        addToSmall: function (queueItem) {
            this.queues.small.push(queueItem);
        },
        addToMediumUp: function (queueItem) {
            this.queues.mediumUp.push(queueItem);
        },
        addToMedium: function (queueItem) {
            this.queues.medium.push(queueItem);
        },
        addToLargeUp: function (queueItem) {
            this.queues.largeUp.push(queueItem);
        },
        check: function () {
            var queries = Foundation.media_queries;
            if (window.matchMedia(queries.xxlarge).matches) this.set("xxlarge");
            else if (window.matchMedia(queries.xlarge).matches) this.set("xlarge");
            else if (window.matchMedia(queries.large).matches) this.set("large");
            else if (window.matchMedia(queries.medium).matches) this.set("medium");
            else if (window.matchMedia(queries.small).matches) this.set("small");
        },
        set: function (width) {
            if (width != this.width) {
                var oldWidth = this.width;
                if (oldWidth === "small") executeQueue(this.queues.mediumUp);
                if (width === "small") executeQueue(this.queues.small);
                else if (width === "medium") executeQueue(this.queues.medium);
                else if (Foundation.utils.is_large_up()) {
                    if (oldWidth === "large" || oldWidth === "xlarge" || oldWidth === "xxlarge") {
                        var noop;
                    } else {
                        executeQueue(this.queues.largeUp);
                    }
                }
                $(window).trigger('switch', [width, oldWidth]);
                $("body").removeClass(removeClassRegexp(/^device-width-/)).addClass("device-width-" + width);
                this.width = width;
            }
        }
    };
    this.components = {
        browserDetect: function () {
            J.browser = bowser;
            addBodyClass("browser-" + convertStringToURLFriendly(bowser.name));
            if (bowser.msie) {
                addBodyClass("msie");
                if (bowser.version == 7) addBodyClass("ie7 msie-old");
                if (bowser.version == 8) addBodyClass("ie8 msie-old");
                if (bowser.version == 9) addBodyClass("ie9");
                if (bowser.version == 10) addBodyClass("ie10");
                if (bowser.version == 11) addBodyClass("ie11");
            } else {
                addBodyClass("not-msie");
            }
            if (bowser.webkit) addBodyClass("webkit");
            if (bowser.blink) addBodyClass("blink");
            if (bowser.gecko) addBodyClass("gecko");
            if (bowser.ios) addBodyClass("ios");
            if (bowser.android) addBodyClass("android");
            if (bowser.mobile) addBodyClass("mobile");
            if (bowser.tablet) addBodyClass("tablet");
            if (bowser.tablet || bowser.mobile || ('ontouchstart' in window)) J.checker.isTouch = true;
        },

        initFoundation: function () {
            $(document).foundation(J.config.foundationConfig);
        },

        registerHandlebarHelpers: function () {
            Handlebars.registerHelper('translate', function (obj) {
                var str = arguments[0];
                return J.translate(str);
            });
            Handlebars.registerHelper('config', function (obj) {
                var str = arguments[0];
                if (str.indexOf(".") !== -1) {
                    var strPart = str.split(".");
                    return J.config[strPart[0]][strPart[1]];
                } else {
                    return J.config[str];
                }
            });
            Handlebars.registerHelper('currentCulture', function () {
                if (J.data.jetshopData) return J.data.culture;
            });
            Handlebars.registerHelper('currentLanguage', function () {
                if (J.data.jetshopData) return J.data.language;
            });
            Handlebars.registerHelper('currency', function (value) {
                if (J.data.jetshopData) {
                    return value + ' ' + J.data.currency.symbol;
                }
                else {
                    return value;
                }
            });
            Handlebars.registerHelper('isVatIncluded', function () {
                if (J.data.jetshopData) return J.checker.isVatIncluded
            });
            Handlebars.registerHelper('isStage', function () {
                if (J.data.jetshopData) return J.checker.isStage;
            });
            Handlebars.registerHelper('unescape', function (obj) {
                if(typeof obj !== "undefined" && obj) {
                    var doc = new DOMParser().parseFromString(arguments[0], "text/html");
                    return doc.documentElement.textContent;
                }
            });
            Handlebars.registerHelper("ifValue", function (conditional, options) {
                if (conditional == options.hash.equals) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            });
            Handlebars.registerHelper("replace", function (str, a, b) {
                // Example: {{{replace ImageUrl.Url "small" "medium"}}}
                if (str && typeof str === 'string') {
                    if (!a || typeof a !== 'string') return str;
                    if (!b || typeof b !== 'string') b = '';
                    return str.split(a).join(b);
                }
            });
            Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
                switch (operator) {
                    case '==':
                        return (v1 == v2) ? options.fn(this) : options.inverse(this);
                    case '===':
                        return (v1 === v2) ? options.fn(this) : options.inverse(this);
                    case '!=':
                        return (v1 != v2) ? options.fn(this) : options.inverse(this);
                    case '!==':
                        return (v1 !== v2) ? options.fn(this) : options.inverse(this);
                    case '<':
                        return (v1 < v2) ? options.fn(this) : options.inverse(this);
                    case '<=':
                        return (v1 <= v2) ? options.fn(this) : options.inverse(this);
                    case '>':
                        return (v1 > v2) ? options.fn(this) : options.inverse(this);
                    case '>=':
                        return (v1 >= v2) ? options.fn(this) : options.inverse(this);
                    case '&&':
                        return (v1 && v2) ? options.fn(this) : options.inverse(this);
                    case '||':
                        return (v1 || v2) ? options.fn(this) : options.inverse(this);
                    default:
                        return options.inverse(this);
                }
            });
            Handlebars.registerHelper('classFriendly', function(v1) {
                var str = arguments[0];
                //Lower case everything
                str = str.toLowerCase();
                //Convert whitespaces and underscore to dash
                str = str.replace(/\s/g, "-");
                return str;
            });
        },

        jetshopLogo: function () {
            $(".infoTextLogo img").attr("src", "/M1/stage/images/responsive-base/powered-by-jetshop-lightgrey.png");
            // Available colors are:
            // powered-by-jetshop-darkgrey.png, powered-by-jetshop-lightgrey.png, powered-by-jetshop-white.png, powered-by-jetshop-black.png
        },
        orderConfirm: function () {

            // RESPONSIVE ORDER CONFIRMATION FIXES

            // GIVE CLASSES TO SUMMARY TABLE CELLS
            var summaryTable = $(".order-details-confirmed-cart-summary");
            summaryTable.find(".order-details-confirmed-cart-summary-header-row, " +
                "tr.order-details-confirmed-cart-summary-specification-row").each(function(){
                var row = $(this);
                row.children().eq(0).addClass("article");
                row.children().eq(1).addClass("qty");
                row.children().eq(2).addClass("price");
                row.children().eq(3).addClass("sum");
            });

            // MAKE LABELS FOR SMALL VERSION
            var qtyLabel = summaryTable.find("th.qty").text().trim();
            var priceLabel = summaryTable.find("th.price").text().trim();
            var sumLabel = summaryTable.find("th.sum").text().trim();

            summaryTable.find("td.qty").prepend("<span class='label'>" + qtyLabel + ": </span>");
            summaryTable.find("td.price").prepend("<span class='label'>" + priceLabel + ": </span>");
            summaryTable.find("td.sum").prepend("<span class='label'>" + sumLabel + ": </span>");

        },

        initCart: function () {
            J.cart = {
                cartId: getCookie("JetShop_CartID")
            };

            // NO COOKIE - POST TO GET ONE
            // if(!J.cart.cartId) {
            //     $.ajax({
            //         type: "POST",
            //         cache: false,
            //         url: J.config.urls.ServicesUrl + "/rest/v2/json/shoppingcart",
            //         success: function (data) {
            //             console.log("cartData: ", data);
            //         },
            //         error: function (jqXHR, textStatus, errorThrown) {
            //             console.error('Ajax Error in initCart:');
            //             console.log(jqXHR);
            //             console.log(textStatus);
            //             console.log(errorThrown);
            //         }
            //     });
            // }

        },

        updateCartData: function (xhr, cartApiData) {
            if (xhr) {
                // ADD DATA ONLY REACHABLE FROM REGULAR CART UPDATE (FREE SHIPPING TEXT)
                var xmlDOM = (new DOMParser()).parseFromString(xhr.responseText, 'text/xml');
                var temp = $(xmlDOM.getElementsByTagName("DivContents")[0].childNodes[1]).text();
                var tempObj = $(temp).text();
                if (tempObj.length > 0) {
                    J.cart.freeFreightMessage = $(temp).text();
                } else {
                    J.cart.freeFreightMessage = false;
                }
            }
            if (cartApiData) {
                // DATA ALREADY EXISTS - UPDATE CART OBJECT
                $.each(cartApiData, function (propName, propValue) {
                    J.cart[propName] = propValue;
                });
                // Trigger event for modules
                $(window).triggerHandler('cart-updated');
            } else {
                // NO EXISTING DATA - GET FROM API
                if(J.cart.cartId) {
                    var restUrl = J.config.urls.ServicesUrl + "/rest/v2/json/" + JetshopData.Culture + "/" + JetshopData.Currency + "/shoppingcart/" + J.cart.cartId;
                    if (J.checker.isLoggedIn) {
                        restUrl += "?pricelistid=" + JetshopData.PriceListId;
                    }
                    $.ajax({
                        type: "GET",
                        cache: false,
                        url: restUrl,
                        success: function (data) {
                            var json = typeof data == 'object' ? data : $.parseJSON('(' + data + ')');
                            cartApiData = json[0];
                            // SEND RECEIVED DATA BACK INTO DATA UPDATE FUNCTION
                            J.components.updateCartData(false, cartApiData);
                        },
                        error: function (request, status, error) {
                            console.error("Cart data fetch error:");
                            console.log(error);
                        }
                    });
                    $(window).triggerHandler('cart-loaded');
                }
            }
        },

        addItemToCart: function (cartItem, callback) {
            // Example cartItem object
            // var cartItem = {
            //     "ProductId": 329,
            //     "AttributeId": 0,
            //     "Quantity": 1
            // };
            var product_json = JSON.stringify(cartItem);
            var restUrl = J.config.urls.ServicesUrl + "/rest/v2/json/shoppingcart/" + J.cart.cartId;
            if (J.checker.isLoggedIn) {
                restUrl += "?pricelistid=" + JetshopData.PriceListId;
            }
            $.ajax({
                type: "POST",
                cache: false,
                url: restUrl,
                data: product_json,
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    Services.cartService.reload();
                    if (callback)
                        callback(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error('Ajax Error in J.components.addItemToCart:');
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        },

        deleteFromCart: function (recId) {
            var restUrl = J.config.urls.ServicesUrl + "/rest/v2/json/" + J.data.culture + "/" + J.data.currency.name + "/shoppingcart/delete/" + J.cart.cartId;
            if (JetshopData.IsLoggedIn) {
                restUrl += "?pricelistid=" + J.data.priceListId;
            }
            $.ajax({
                type: "POST",
                cache: false,
                url: restUrl,
                data: '{ "RecId": ' + recId + '}',
                dataType: 'json',
                contentType: 'application/json',
                success: function (data) {
                    var json = typeof data == 'object' ? data : $.parseJSON('(' + data + ')');
                    var cartApiData = json[0];
                    J.components.updateCartData(false, cartApiData);
                    Services.cartService.reload();
                    if (J.checker.isCheckoutPage) {
                        location.reload();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.error('Ajax Error in J.components.deleteFromCart:');
                    console.log(jqXHR);
                    console.log(textStatus);
                    console.log(errorThrown);
                }
            });
        },

        emptyCart: function () {
            if (J.cart.ProductsCart.length > 0) {
                for (var item in J.cart.ProductsCart) {
                    J.components.deleteFromCart(J.cart.ProductsCart[item]["RecId"]);
                }
            }
        }
    };
    this.pages = {
        allPages: function () {
            J.components.initCart();
            J.api.helpers.initCache();
        },
        queues: {
            "all-pages": [],
            "orderconfirmation-page": [],
            "checkout-page": [],
            "standard-page": []
        },
        addToQueue: function (pageType, queueItem) {
            if (typeof J.pages.queues[pageType] != "undefined") {
                J.pages.queues[pageType].push(queueItem);
            } else {
                console.log("Error: Queue-type not found: " + pageType);
            }
        },
        init: function () {
            J.pages.allPages();
            executeQueue(J.pages.queues["all-pages"]);
            if (typeof J.pages.queues[J.pageType] != "undefined") {
                executeQueue(J.pages.queues[J.pageType]);
            } else {
                console.log("Error: Queue-type not found: " + J.pageType);
            }
        }
    };

    this.api = {
        helpers: {
            baseUrl: function (includeCulture, includeCurrency) {
                var portNumber = "";
                if (window.location.port.length) {
                    portNumber = ":" + window.location.port;
                }
                var url = window.location.protocol + "//" + window.location.hostname + portNumber + J.config.urls.ServicesUrl + "/Rest/v2/json/";
                if (includeCulture) url += J.data.culture + "/";
                if (includeCurrency) url += J.data.currency.name + "/";
                return url;
            },
            //TODO when adding cart-services we need to add method postAjax
            getAjax: function (service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency) {
                //console.info("Actually fetching AJAX: " + service);
                $.ajax({
                    type: "GET",
                    url: J.api.helpers.baseUrl(includeCulture, includeCurrency) + service,
                    success: function (data, textStatus, jqXHR) {
                        if (enableCache) {
                            J.api.helpers.save(J.api.helpers.baseUrl(includeCulture, includeCurrency) + service, cacheMinutes, data);
                        }
                        if (typeof callback == "function") {
                            callback(data, callbackOptions);
                        } else if (typeof callback.success == "function") {
                            callback.success(data, callbackOptions, textStatus, jqXHR);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        var error = {"error": "Ajax fail", "service": service};
                        J.api.helpers.raiseError(error);
                        if (typeof callback.error == "function") {
                            callback.error(callbackOptions, xhr, ajaxOptions, thrownError);
                        }
                    },
                    complete: function (event, xhr) {
                        if (typeof callback.complete == "function") {
                            var data = JSON.parse(event.responseText);
                            callback.complete(data, callbackOptions, event, xhr);
                        }
                    }
                });
            },
            getter: function (service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency) {
                if (typeof callback === "undefined") {
                    var error = {"error": "No callback", "service": service};
                    J.api.helpers.raiseError(error);
                } else {
                    if (typeof callback.before == "function") {
                        callback.before(callbackOptions);
                    }
                    if (enableCache) {
                        var itemId = J.api.helpers.baseUrl(includeCulture, includeCurrency) + service;
                        if (J.api.helpers.read(itemId, cacheMinutes)) {
                            var cachedItem = J.api.helpers.read(itemId, cacheMinutes);
                            var minutes = Math.floor(Math.abs(cachedItem.timeStamp - Date.now()) / 60000);
                            if (minutes < cacheMinutes) {
                                var jqXHR = cachedItem.data;
                                var textStatus = "success";
                                if (typeof callback == "function") {
                                    callback(cachedItem.data, callbackOptions);
                                } else if (typeof callback.success == "function") {
                                    callback.success(cachedItem.data, callbackOptions, textStatus, jqXHR);
                                }
                                if (typeof callback.complete == "function") {
                                    callback.complete(cachedItem.data, callbackOptions, textStatus);
                                }
                            } else {
                                J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                            }
                        } else {
                            J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                        }
                    } else {
                        J.api.helpers.getAjax(service, callback, callbackOptions, enableCache, cacheMinutes, includeCulture, includeCurrency);
                    }
                }
            },
            read: function (id, cacheMinutes) {
                var item = localStorage.getItem(J.config.apiCachePrefix + id);
                if (item === "") {
                    return false;
                } else {
                    return JSON.parse(item);
                }
            },
            save: function (id, cacheMinutes, data) {
                //log("Caching item: " + id);
                var storage = isLocalStorageNameSupported();
                if (storage) {
                    var timeStamp = Date.now();
                    var item = {"cacheMinutes": cacheMinutes, "timeStamp": timeStamp, "data": data};
                    try {
                        localStorage.setItem(J.config.apiCachePrefix + id, JSON.stringify(item));
                    } catch (e) {
                        if (J.api.helpers.isQuotaExceeded(e)) {
                            console.log("Error storing local storage");
                        }
                    }
                }
            },
            clearCache: function () {
                for (var key in window.localStorage) {
                    if (key.indexOf(J.config.apiCachePrefix) != -1) {
                        localStorage.removeItem(key);
                    }
                }
                console.log("Cleared cache.");
            },
            clearOutdatedCache: function () {
                var seconds = 60;
                var milliseconds = 1000;
                var now = Date.now();
                for (var key in window.localStorage) {
                    if (key.indexOf(J.config.apiCachePrefix) != -1) {
                        var item = window.localStorage[key];
                        var localStoragePostTimestamp = JSON.parse(item).timeStamp;
                        var localStoragePostMinutes = JSON.parse(item).cacheMinutes;
                        var timestampWithMinutes = localStoragePostTimestamp + (localStoragePostMinutes * seconds * milliseconds)

                        if (timestampWithMinutes < now) {
                            delete localStorage[key];
                        }
                    }
                }
            },
            initCache: function () {
                if ($(".vat-selector-input input").length > 0) {
                    var vatInputElementId = $(".vat-selector-input input").attr("id");
                    var vatInputElement = document.getElementById(vatInputElementId);
                    vatInputElement.onclick = function () {
                        J.api.helpers.clearCache();
                        setTimeout('__doPostBack(\'' + vatInputElementId + '\',\'\')', 0);
                    }
                }
            },
            raiseError: function (error) {
                console.error(error);
            },
            isQuotaExceeded: function (error) {
                var quotaExceeded = false;
                if (error) {
                    if (error.code) {
                        switch (error.code) {
                            case 22:
                                quotaExceeded = true;
                                break;
                            case 1014:
                                // Firefox
                                if (error.name === 'NS_ERROR_DOM_QUOTA_REACHED') {
                                    quotaExceeded = true;
                                }
                                break;
                        }
                    } else if (error.number === -2147024882) {
                        // Internet Explorer 8
                        quotaExceeded = true;
                    }
                }
                return quotaExceeded;
            },
            createObject: function (data) {
                return {
                    TotalProducts: data.length,
                    TotalPages: 1,
                    ProductsInPage: data.length,
                    ProductItems: data,
                    PageSize: 0
                };
            }
        },
        product: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                if (typeof id === "number") {
                    var service = "products/" + id;
                    J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes);
                } else {
                    var list = id;
                    var products = [];
                    var add = function (data) {
                        products.push(data.ProductItems[0]);
                        if (list.length == products.length) {
                            var obj = J.api.helpers.createObject(products);
                            callback(obj, callbackOptions);
                        }
                    };
                    for (var i = 0; i < list.length; i++) {
                        J.api.product.get(add, i, enableCache, cacheMinutes, list[i]);
                    }
                }
            },
            getRelatedProducts: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                var products = [];
                var total = 0;
                var get = function (productData, id) {
                    var list = productData.ProductItems[0].RelatedProducts;
                    total = list.length;
                    for (var i = 0; i < list.length; i++) {
                        J.api.product.get(add, null, enableCache, cacheMinutes, list[i]);
                    }
                };
                var add = function (data) {
                    products.push(data.ProductItems[0]);
                    if (total == products.length) {
                        callback(products, callbackOptions);
                    }
                };
                J.api.product.get(get, id, enableCache, cacheMinutes, id);
            }
        },
        category: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id, displayTypeWeb) {
                var service = "categories/" + id;
                if (displayTypeWeb) service += "?displayType=web";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            },
            getProducts: function (callback, callbackOptions, enableCache, cacheMinutes, id, full, rows, page) {
                var service = "categories/" + id + "/products";
                if (full) service += "/full";
                if (rows) service += "/" + rows;
                if (page) service += "/" + page;
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, true);
            },
            list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "categories";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            }
        },
        page: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                var service = "pages/" + id;
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            },
            list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "pages";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            }
        },
        search: function (callback, callbackOptions, enableCache, cacheMinutes, string, full, rows, page) {
            var service = "search/";
            if (full) {
                service += "full/" + string;
            } else {
                service += string;
            }
            if (rows) service += "/" + rows;
            if (page) service += "/" + page;
            J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
        },
        settings: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "settings";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
            }
        },
        startpage: {
            get: function (callback, callbackOptions, enableCache, cacheMinutes, id) {
                var service = "customstartpage/" + id;
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, true, false);
            },
            list: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "customstartpages";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
            },
            active: function (callback, callbackOptions, enableCache, cacheMinutes) {
                var service = "activestartpage";
                J.api.helpers.getter(service, callback, callbackOptions, enableCache, cacheMinutes, false, false);
            }
        },
        // TODO: Incomplete function
        filter: {
            get: function(postData, callback, callbackOptions, categoryId) {
                var service = "filters/"+ categoryId;
                // TODO: Create a popster method
                // J.api.helpers.poster(service, postData, callback, callbackOptions, categoryId);
            }
        }
    };

    ////////////////////////////////////////////////////////////////////////////////////////////////////
    //
    //  Method calls, init and setup
    //
    if (typeof JetshopData !== "undefined") {
        this.data.channelInfo = JetshopData.ChannelInfo;
        this.data.language = JetshopData.Language;
        this.data.culture = JetshopData.Culture;
        this.data.priceList = JetshopData.PriceList;
        this.data.priceListId = JetshopData.PriceListId;
        this.data.categoryId = JetshopData.CategoryId;
        this.data.productId = JetshopData.ProductId;
        this.data.pageId = JetshopData.PageId;
        this.checker.isStage = JetshopData.IsStage;
        this.checker.isLoggedIn = JetshopData.IsLoggedIn;
        this.checker.isVatIncluded = JetshopData.VatIncluded;
        this.checker.isCheckoutHttps = JetshopData.IsCheckoutHttps;
        this.data.currency = {
            name: JetshopData.Currency,
            separator: JetshopData.CurrencyDecimalSeparator,
            display: JetshopData.CurrencyDisplay,
            symbol: JetshopData.CurrencySymbol
        };
        this.data.jetshopData = true;
        if (typeof JetshopData.Urls !== "undefined") {
            this.config.urls = JetshopData.Urls;
        }
    } else {
        J.data.channelInfo = {
            Active: 1,
            Data: {
                1: {
                    Cultures: [
                        "da-DK",
                        "de-DE",
                        "en-GB",
                        "fi-FI",
                        "nb-NO",
                        "sv-SE"
                    ],
                    Currencies: [
                        "SEK"
                    ],
                    DefaultCulture: "sv-SE",
                    DefaultCurrency: "SEK",
                    Name: "SV"
                }
            },
            Total: 1
        };
        J.data.language = "en";
        J.data.categoryId = 0;
        J.data.productId = 0;
        J.data.pageId = 0;
    }
    this.pageType = this.setPageType();
    //
    //  Initialize
    //
    this.init = function () {
        addBodyClass(this.pageType);
        J.api.helpers.clearOutdatedCache();
        J.components.initFoundation();
        J.components.registerHandlebarHelpers();
        J.components.browserDetect();
        J.pages.init();
        J.switch.check();
        J.deviceWidth = J.switch.width;
        $(document).foundation('reflow');

        // Bind detection of cart update
        $(document).ajaxComplete(function (event, xhr, settings) {
            var data = {};
            if (settings.url === J.config.urls.CartServiceUrl && xhr.readyState == 4 && xhr.status == 200) {
                J.components.updateCartData(xhr, false);
            }
        });

        //$(window).trigger('resize');

        // Prevent J.init function from firing twice
        J.init = function () {
            console.error("J.init() triggered twice. Check base.master & script management!");
        };
    };
};

// On resize
$(window).resize(function () {
    J.switch.check();
    J.deviceWidth = J.switch.width;
});

$(document).ready(function () {
    J.init();
});
