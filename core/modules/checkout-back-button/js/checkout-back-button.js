var checkoutBackButton = {
    init: function () {

        checkoutBackButton.addTranslations();

        if($(".empty-cart-wrapper").length) {
            $("html").addClass("empty-cart-checkout")
        }

        // BIND DOM CHANGES IN CART TO RUN REWRITECHECKOUT
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_endRequest(function () {
            checkoutBackButton.rewriteResponsiveCheckout();
        });
        checkoutBackButton.rewriteResponsiveCheckout();


    },

    rewriteResponsiveCheckout: function(){
        var checkoutHeaderContainer = $("#ctl00_main_responsivecheckout_UpCheckoutHeader");
        var checkoutHeader = checkoutHeaderContainer.find("h1");
        if(!checkoutHeaderContainer.find(".checkout-top-links").length) {
            var topLinkContainer = $("<div class='checkout-top-links'></div>");
            checkoutHeader.find("a").appendTo(topLinkContainer);
            topLinkContainer.prepend("<a class='button checkout-back-button' href='" + JetshopData.Urls.CountryRootUrl + "'>"
                + J.translate("checkoutBackButtonText") + "</a>");
            checkoutHeaderContainer.append(topLinkContainer);
        }
    },
    addTranslations: function () {
        J.translations.push({
            checkoutBackButtonText: {
                sv: "Fortsätt handla",
                nb: "Fortsette å handle",
                da: "Fortsætte med at handle",
                fi: "Jatka ostoksia",
                de: "Mit dem Einkaufen fortfahren",
                en: "Continue shopping"
            }
        });
    }


};

J.pages.addToQueue("checkout-page", checkoutBackButton.init);
