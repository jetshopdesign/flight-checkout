var checkoutValidation = {
    config: {
        errorDivOffset: 2
    },
    init: function () {
        // Only if responsive checkout
        if (typeof Checkout != "undefined") {
            if (typeof (Page_ClientValidate) != "undefined") {
                ValidatorUpdateDisplay = checkoutValidation.validate;
            }
        }

        // // TEST
        // var view = J.views['checkout-validation/checkout-validation'];
        // $("body").append(view);
    },
    validate: function (val) {
        var that = $(val);
        if (val.isvalid) {
            that.hide();
        } else {
            that.fadeIn('fast');
            var labelHeight = that.outerHeight();
            var inputHeight = that.parent().find(".form-text").outerHeight();
            var setHeight = labelHeight + inputHeight + checkoutValidation.config.errorDivOffset;
            that.css("margin-top", -setHeight);
            that.parent().find(".form-text").addClass("active-error").focus(function () {
                checkoutValidation.clearError($(this));
            }).on("input", function () {
                checkoutValidation.clearError($(this));
            });
        }
    },
    clearError: function (that) {
        that.removeClass("active-error");
        that.parent().find(".customer-info-input-error").hide();
    }
};

J.pages.addToQueue("checkout-page", checkoutValidation.init);
