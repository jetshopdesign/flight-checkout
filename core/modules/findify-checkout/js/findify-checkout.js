var findifyCheckout = {
  config: {
    findifyAccounts: { // FILL IN FINDIFY ID PER CHANNEL
      channel_1: 'XXXXX',
      channel_2: 'XXXXX'
    },
    recommendations: {
      useRecommendations: false,
      recommendationId: 'cart-findify-rec-10',
      prependToSelector: '#main-area > .content'
    }
  },
  init: function () {

    // GET CORRECT FINDIFY ID
    var channelId = JetshopData.ChannelInfo.Active;
    var accountId =
      findifyCheckout.config.findifyAccounts[
      'channel_' + channelId
        ];

    // ONLY ACTIVATE ON CHANNELS WITH VALID ACCOUNT ID
    if (accountId) {

      if ($("html").attr("class").match(/checkout/)) { // CHECKOUT PAGE

        // INSERT RECOMMENDATION
        if (findifyCheckout.config.recommendations.useRecommendations) {
          findifyCheckout.insertCheckoutRecommendations();
        }
      }
      else { // ORDER CONFIRMATION PAGE

        // BUILD & INSERT PURCHASE TRACKING ELEMENTS
        findifyCheckout.insertPurchaseTrackingElements();
      }

      // SET SCRIPT SRCS
      var findifyScriptSrc = '//assets.findify.io/' + accountId + '.min.js';

      // INSERT SCRIPT
      findifyCheckout.insertFdScript(findifyScriptSrc);

    }
    else {
      console.log("No Findify account id defined for channel.");
    }

  },
  insertCheckoutRecommendations: function () {
    $(findifyCheckout.config.recommendations.prependToSelector).prepend("<div class='findify-element' id='cart-findify-rec-10' />")
  },
  insertPurchaseTrackingElements: function () {
    var orderData = window.dataLayer[0];
    var orderId = orderData.transactionId;
    var currency = orderData.transactionCurrency;
    var revenue = parseFloat(orderData.transactionValue.replace(',', '.'));
    var totalDiscount = '0'; // WE CANNOT GET THIS DATA ON ORDER CONFIRMATION PAGE
    var totalTax = parseFloat(orderData.transactionVat.replace(',', '.'));
    var totalShipping = parseFloat(
      orderData.transactionShipping.replace(',', '.')
    );

    // WRAPPER ELEMENT
    var findifyWrapper = $("<div data-findify-event='purchase' />");
    findifyWrapper.attr('data-findify-order-id', orderId);
    findifyWrapper.attr('data-findify-currency', currency);
    findifyWrapper.attr('data-findify-revenue', revenue);
    findifyWrapper.attr('data-findify-total-discount', totalDiscount);
    findifyWrapper.attr('data-findify-total-tax', totalTax);
    findifyWrapper.attr('data-findify-total-shipping', totalShipping);

    // ALL PRODUCTS
    $.each(orderData.transactionProducts, function (index, prodData) {
      var itemId = prodData.sku;
      var variantId = prodData.sku;
      var unitPrice = prodData.price.replace(',', '.');
      var qty = prodData.qty;

      var prodItem = $('<div />');
      prodItem.attr('data-findify-item-id', itemId);
      prodItem.attr('data-findify-variant-item-id', variantId);
      prodItem.attr('data-findify-unit-price', unitPrice);
      prodItem.attr('data-findify-quantity', qty);

      prodItem.appendTo(findifyWrapper);
    });

    findifyWrapper.appendTo('body');
  },
  insertFdScript: function (findifyScriptSrc) {
    var findifyScript = $("<script src='" + findifyScriptSrc + "' async defer />");
    $('head').append(findifyScript);
  }
};

$(document).ready(function () {
  findifyCheckout.init();
});