
# Jetshop Flight Checkout (Responsive Base)

This is a trimmed-down and partly rewritten version of the Responsive Base Checkout, intended for use with all Flight shops until the native React checkout is launched. 

Please note: Use Node version 10.23.1 for guaranteed functionality.


## Main changes and other noteworthy stuff
* All code and files not related to the Checkout and Order confirmation pages have been stripped out
* Updated and overhauled design (it's still the old checkout though, don't expect miracles)
* Variables for all basic checkout design decisions are provided at the top of base_settings.scss, so that standard design implementations can be done in a matter of minutes
* Instead of many different files, there is one script file and one css file to load (the old script files are generated separately and then concatenated into one production file).
* Support for modules still intact.
* Only runs on Gulp (no support for Grunt)
* Intended to run on /checkout, not /mobilecheckout. Ask the support team to change this setting for the store if it is not correct.
* Installation, repo handling and general development follows Responsive Base. The main difference is less files and less source code, and only two exported files instead of six.
* Any questions or corrections? Contact jan.sjogren@jetshop.se 


## Old Responsive Base readme below

Welcome to the repository for the Jetshop Responsive Base.
 
This is a custom framework for building responsive design on the Jetshop E-Commerce Platform Version 68.1.
 
The framework uses Grunt to manage Sass & JS, in an efficient manner; complete with auto-deployment to your web shop.

If your project are using channels, please contact Jetshop. All instructions are for channel M1 only.
  
*For more information see the list of resources below:*

|       |  
| ----- | ----- 
| **Report issues**                     | [bitbucket.org/jetshopdesign/responsive-base/wiki/issues](https://bitbucket.org/jetshopdesign/responsive-base/issues)   
| **Documentation**                     | [bitbucket.org/jetshopdesign/responsive-base/wiki/documentation/home](https://bitbucket.org/jetshopdesign/responsive-base/wiki/documentation/home)     
| **Release notes**                     | [bitbucket.org/jetshopdesign/responsive-base/src/master/releasenotes.md](https://bitbucket.org/jetshopdesign/responsive-base/src/master/releasenotes.md)
| **Wireframes**                        | [bitbucket.org/jetshopdesign/responsive-base-wireframes](https://bitbucket.org/jetshopdesign/responsive-base-wireframes)
| **Installed development version**     | [responsive-base.jetshop.se](http://responsive-base.jetshop.se/)
| **Demo**                              | [demostore.jetshop.se](http://demostore.jetshop.se)
    
    
## __Install for new shop setup:__

*Before first install, please read the documentation on [File structure](https://bitbucket.org/jetshopdesign/responsive-base/wiki/documentation/filestructure)*


> ### Note: Using gulp or grunt?

> We have two separate build-systems, gulp & grunt.

> Gulp is used by Elastic-customers on the FTP Secure setup, all other project should continue using grunt.

> When in doubt, use grunt.

> If you are using gulp, please replace all usage of grunt with gulp.


### Technical preparation - Computer

*This preparation need to be done once per computer*

1. Install Node to get npm: [http://nodejs.org](http://nodejs.org/)

1. Install Git: [http://git-scm.com/download/](http://git-scm.com/download/) 

1. Make sure that Git have user & email properly setup: [https://help.github.com/articles/set-up-git/](https://help.github.com/articles/set-up-git)

Using Grunt:

4. Run ``sudo npm install -g grunt`` in terminal.

5. Run ``sudo npm install -g grunt-cli`` in terminal.

Using Gulp:

4. Run ``sudo npm install -g gulp`` in terminal.

5. Run ``sudo npm install -g gulp-cli`` in terminal.

Windows-users: Remove the word 'sudo'    


### 1. Technical preparation - Project

1. Create a local folder to hold the project.

1. Login to [bitbucket.org](https://bitbucket.org/)

1. Clone repo from [bitbucket.org/jetshopdesign/responsive-base](https://bitbucket.org/jetshopdesign/responsive-base) into local folder.

1. After this step you will have a new sub-folder called ``responsive-base`` inside your local folder. 

1. Rename the sub-folder called ``responsive-base`` to ``customer-customername``. 

> **IMPORTANT** *The sub-folder ``customer-customername`` is now your working folder. All references in this guide will start at this folder.*.


### 2. Remove .git folder
                                  
1. Start terminal, cd to your working folder ``customer-customername``.

1. Remove .git folder ``rm -rf .git`` (or delete in Windows Explorer).

### 3. Create a new .git folder

1. In terminal, initialize a new Git repository and add all files: ``git init; git add .``

1. Make an initial commit ``git commit -a -m "First commit"``  

### 4. Rename files & update paths

*This step is needed for Jetshop Support*

1. Rename file ``stage/scripts/client.js`` to ``stage/scripts/clientName.js``. Name the file using camelCase, example: kallesFiskeButik.js 

1. Update path to this file in ``core/pages/base.master.htm`` (search for 'client.js'). 

### (5. Only for designpartners: Prepare a local repo, hook it up with BitBucket) 

1. Have an admin create an empty repo at [bitbucket.org/jetshopcustomers](https://bitbucket.org/jetshopcustomers/)
 
1. Connect your local repo with the newly created repo on BitBucket (follow instructions on BitBucket for "I have an existing project").

### 6. Install binaries to handle automation

1. Run ``npm install`` in terminal.
 

### 7. Create a package to restore in Jetshop Admin

1. Run ``grunt package`` in terminal.
 
1. This will create a zip-file in folder ``package``
 
1. Login to Jetshop Admin, navigate to Appearance > Templates.
 
1. Use the Restore/Import function with the newly created package.


### 8. Setup FTP username & password for automatic deployment


> Note: For gulp this file is called .ftpconfig.js. Rename the file .ftpconfig.js.sample and edit

1. Create a file called ``.ftppass`` with the following content:

```
#!js
{
   "key1": {
     "username": "storename.jetshop.se",
     "password": "PASSWORD"
   }
}
```
      
Windows-users: Create a file called ".ftppass." Windows will remove the last dot.    

Windows-users: Make sure File Explorer are displaying File Name Extensions.    
    
Username & password are found in Jetshop Admin > Appearance > Templates > FTP.
 
*Do not replace the object name "key1".*

### 9. Run grunt and start deploying 

1. Run ``grunt`` in terminal and then fetch some coffee, the initial upload will take a while.

1. If you get FTP errors from Grunt, just redo the command ``grunt`` again and again...


### 10. Move script-block to Script Management

1. In Jetshop Admin > Appearance > Templates, open ``base.master.htm`` and follow instructions for "Move this block".

1. Follow instructions for "Settings in admin" below.

1. Start working :)

***


# Settings in admin

### Product images need rescaling

New widths for product-images, old ones in parenthesis:

* Large  : 800px (460px)
* Medium : 600px (340px)
* Small  : 400px (220px)
* Thumbs : 220px (100px)


# Post-actions after development is done

Disable the script-handling by commenting that block, and uncomment the regular include block (Read more: https://bitbucket.org/jetshopdesign/responsive-base/wiki/documentation/script-handling).

Make sure all material are added to ``material`` folder. This means vector-logos, images, Photoshop-files, design guides, documentation etc.

(Push all changes to repo.)


