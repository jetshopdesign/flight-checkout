//
//  GET /troubleshooting page
//
var fs = require("fs");
var path = require("path");
require("../node-functions.js");

exports.index = function(req, res){
    res.render('troubleshooting', {
        layout: false,
        title: "Tools",
        ftpPass: getFtpPass(),
        packageData: getPackageJson(),
        npmVersion: getNpmVersion(),
        gruntVersion: getGruntVersion(),
        gitVersion: getGitVersion(),
        global: global
    });
};

